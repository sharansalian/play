import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun CountdownLinearProgressBar(durationMillis: Long, color: Color, height: Dp = 4.dp) {
    var progress by remember { mutableStateOf(1f) }
    val scope = rememberCoroutineScope()

    LaunchedEffect(durationMillis) {
        scope.launch {
            for (i in 0..100) {
                progress = 1 - i / 100f
                delay(durationMillis / 100)
            }
        }
    }

    LinearProgressIndicator(
        progress = progress,
        color = color,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
            .height(height)
    )
}

@Composable
@Preview
fun PreviewCountdownLinearProgressBar() {
    CountdownLinearProgressBar(durationMillis = 5 * 60 * 1000, color = Color.Blue)
}