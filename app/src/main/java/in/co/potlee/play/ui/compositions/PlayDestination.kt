package `in`.co.potlee.play.ui.compositions

import HomeScreen
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import `in`.co.potlee.play.ui.compositions.screen.ChatScreen
import `in`.co.potlee.play.ui.compositions.screen.SettingsScreen

/**
 * Contract for information needed on every navigation destination
 */
interface PlayDestination {
    val icon: ImageVector
    val route: String
    val screen: @Composable () -> Unit
}

object Home : PlayDestination {
    override val icon = Icons.Filled.Home
    override val route = "home"
    override val screen: @Composable () -> Unit = { HomeScreen() }
}

object Chat : PlayDestination {
    override val icon: ImageVector = Icons.Filled.Email
    override val route: String = "chat"
    override val screen: @Composable () -> Unit = { ChatScreen() }
}

object Settings : PlayDestination {
    override val icon: ImageVector = Icons.Filled.Settings
    override val route: String = "settings"
    override val screen: @Composable () -> Unit = { SettingsScreen() }
}