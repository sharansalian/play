import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter

@Composable
fun ImageSection(index: Int, total: Int) {
    val bannerUrl = "https://cdn.pixelbin.io/v2/potlee/original/public/banners/banner-4.png"
    Box(
        modifier = Modifier
            .width(200.dp)
            .height(150.dp)
            .padding(
                start = if (index == 0) 0.dp else 8.dp,
                end = if (index == total) 0.dp else 8.dp,
            )
            .background(Color.Transparent)
    ) {
        // Load the image using Coil's rememberImagePainter
        val painter = rememberImagePainter(
            data = bannerUrl,
            builder = {
                crossfade(true)
            }
        )
        androidx.compose.foundation.Image(
            painter = painter,
            contentDescription = "Banner",
            modifier = Modifier.fillMaxSize()
        )
    }
}
