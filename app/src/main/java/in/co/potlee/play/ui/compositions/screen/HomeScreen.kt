import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun HomeScreen() {
    val images = 5
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        items(10) { columnIndex ->
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.LightGray)
                    .padding(8.dp)
            ) {
                Text("Column $columnIndex", style = MaterialTheme.typography.headlineMedium)

                if (columnIndex == 3) {
                    LazyRow {
                        items(images) { index ->
                            ImageSection(
                                index =  index,
                                total = images - 1
                            )
                        }
                    }
                } else {
                    LazyRow(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 8.dp)
                    ) {
                        items(20) { sectionIndex ->


                            val sectionBackgroundColor =
                                if (sectionIndex % 2 == 0) Color.LightGray else Color.Red
                            val sectionPadding = if (sectionIndex % 2 == 0) 8.dp else 16.dp

                            Box(
                                modifier = Modifier
                                    .width(120.dp)
                                    .height(80.dp)
                                    .background(sectionBackgroundColor)
                                    .border(1.dp, Color.Black)
                                    .padding(sectionPadding)
                            ) {
                                Text("Section $sectionIndex")
                            }
                        }
                    }
                }
            }
        }
    }
}
